#!/bin/sh -e

# Take a screenshot
scrot /tmp/screen.png

# Pixellate it 10x
mogrify -scale 10% -scale 1000% /tmp/screen.png
convert -composite /tmp/screen.png ~/.i3/doc.png -gravity South -geometry -20x1200 /tmp/screen_locked.png


# Lock screen displaying this image.
i3lock -i /tmp/screen_locked.png

# Turn the screen off after a delay.
sleep 60; pgrep i3lock && xset dpms force off
